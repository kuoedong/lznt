![LZNT](https://image.kuo.cool/lznt.png "LOGO")


# LZNT

#### Description
An IQ indicator based on java

#### Instructions

1.  git clone https://gitee.com/kuoedong/lznt.git


#### Contribution

1.  Fork the repository
2.  Commit your code
3.  Create Pull Request


#### Feature

1.  Putting unpleasant things into code
2.  Adopting the broadest open source approach to MIT

#### To-do
- Optimize code to make it cleaner
- Add GUI interface
- Make code more flexible
