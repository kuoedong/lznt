![LZNT](https://image.kuo.cool/lznt.png "LOGO")


# LZNT(李真脑瘫)
                                                                                        
                                                                                          
#### 介绍
Java编写的IQ indicator


#### 使用说明

1.  git clone https://gitee.com/kuoedong/lznt.git


#### 如何参与贡献

1.  Fork 本仓库
2.  提交代码
3.  新建 Pull Request


#### 关于

1.  将不愉悦的东西写进代码
2.  采用最宽泛的开源方式MIT

#### To-do 
- 优化代码，让代码更简洁
- 加入GUI界面
- 让代码更灵活
